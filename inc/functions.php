<?php

function cutString($text, $max_length = 0, $end = '...', $sep = '[@]') {
	// Si la variable $max_length est définie, supérieure à 0
	// Et que la longueur de la chaîne $text est supérieure à max_length
	if($max_length > 0 && strlen($text) > $max_length) {
		// On insère une châine dans le texte tous les X caractères sans couper les mots
		$text = wordwrap($text, $max_length, '[@]');
		// On découpe notre chaîne en plusieurs bouts répartis dans un tableau
		$text = explode('[@]', $text);
		// On retourne le premier élément du tableau concaténé avec la chaîne $end
		return $text[0].$end;
	}
	// On retourne la chaîne de départ telle quelle
	return $text;

}
