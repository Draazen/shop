            <div class="col-md-3">

                <p class="lead">Categories</p>
                <div class="list-group">
                    <a href="#" class="list-group-item active">Category 1</a>
                    <a href="#" class="list-group-item">Category 2</a>
                    <a href="#" class="list-group-item">Category 3</a>
                </div>

                <p class="lead">Related products</p>

                <?php 

                $query = $db->prepare('SELECT * FROM products WHERE category = :category LIMIT 3');
                $query->bindValue('category', $product['category'], PDO::PARAM_INT);
                $query->execute();
                $related_products = $query->fetchAll();


                foreach($related_products as $key => $related_product) {

                ?>

                <div class="product">
                    <div class="thumbnail">
                        <img src="http://placehold.it/320x150" alt="">
                        <div class="caption">
                            <h4 class="pull-right"><?= $related_product['price'] ?> &euro;</h4>
                            <h4><a href="#"><?= $related_product['name'] ?></a>
                            </h4>
                            <p><?= cutString($related_product['description'], 100) ?></p>
                        </div>
                        <div class="ratings">
                            <p class="pull-right">12 reviews</p>
                            <p>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star"></span>
                                <span class="glyphicon glyphicon-star-empty"></span>
                            </p>
                        </div>
                        <div class="btns clearfix">
                            <a class="btn btn-info pull-left" href=""><span class="glyphicon glyphicon-eye-open"></span> View</a>
                            <a class="btn btn-primary pull-right" href=""><span class="glyphicon glyphicon-shopping-cart"></span> Add to cart</a>
                        </div>
                    </div><!-- /.thumbnail -->
                </div><!-- /.product -->

                <?php } ?>

            </div><!-- /.col-md-3 -->
